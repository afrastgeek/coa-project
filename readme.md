# Computer Organization and Architecture Project

COA Project

Hosted on http://afrastgeek.gitlab.io/coa-project/

---

Project by group 10 of Computer Organization and Architecture (IK420) Class.

Group member (sorted alphabetically):
- Arga Sanjaya [1507545],
- Irsam Rahmat Yusuf [1200306],
- M Ammar Fadhlur Rahman [1507506],
- Nopen Ariko [1506931],
- Rizki Nugraha [1506748],
- Vivin Meyske Rudamaga [1306040]

## Feature

### MIPS Calculator

An Instruction rate calculator.

## How-to

Open `index.html` with web browser to try.

### Developer's How-to

To edit/develop/maintain this project:
- clone/fork this repo
- `npm install`
- `bower install`
- `gulp`

## License

See `LICENSE`

## TO-DO

Things to-do:
- Add more feature.