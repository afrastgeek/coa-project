new Vue({
  el: '#app',
  validators: { // `positive` custom validator is local registration
    positive: function (val/*,rule*/) {
      return /^[+]?([.]\d+|\d+[.]?\d*)$/.test(val);
    },
  },
  data: {
    title: 'Instruction Execution Rate Calculator',
    subtitle: 'Group 10 | KOM-2C1 | IK420',
  },
  methods: {
    mips: function (dec) {
      /* mips are 1 per instruction time * 10^-9 (ns) * 10^6 (mega) */
      return (1 / (dec * 0.000000001 * 1000000));
    },
    damnedNegative: function() {
      var v = $(this).val(),
            t = parseInt(v,10),
            b = isNaN(t);
        if (b){
            $(this).val('');
        }
        else {
            $(this).val(Math.abs(t));
        }
    }
  }
});