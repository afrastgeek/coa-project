var gulp = require('gulp');

var jshint = require('gulp-jshint');

gulp.task('copy-js', function() {
  gulp.src([
    './bower_components/vue/dist/vue.min.js',
    'bower_components/vue-validator/dist/vue-validator.min.js'
  ]) 
  .pipe(gulp.dest('assets/js/vendor'));
});

gulp.task('lint', function() {
  return gulp.src('assets/js/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

gulp.task('default', ['copy-js', 'lint']);